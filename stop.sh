#!/bin/bash

SELFPATH=$(dirname $(readlink -f $0))
kill -INT $(cat ${SELFPATH}/log/server.pid) && echo "kztong-cart stopped."
rm ${SELFPATH}/log/server.pid
