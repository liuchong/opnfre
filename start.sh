#!/bin/bash

PROJECT_NAME="opnfre"
PORT=3005

cd $(dirname $(readlink -f $0))

if [ -e log/server.pid ]; then
  echo "${PROJECT_NAME} already running, PID: $(cat log/server.pid)"
  exit 0
fi

nohup rails server --port=$PORT --binding=127.0.0.1 \
  --environment=development \
  >> log/server.out 2>> log/server_error.out &

if [ "$?" == "0" ]; then
  echo "${PROJECT_NAME} start, PID: $!"
  echo $! > log/server.pid
else
  echo "!!! ${PROJECT_NAME} start fail: $? !!!"
  echo "-1" > log/server.pid
fi
